package ds.project.assignment1.validators;

public interface FieldValidator<T> {
    boolean validate(T t);
}
