import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Drug } from '../_models/drug';

@Injectable({
  providedIn: 'root'
})
export class DrugService {
  baseUrl = environment.apiUrl + 'drug/';

constructor(private http: HttpClient) { }


  getDrugs(): Observable<Drug[]> {
    return this.http.get<Drug[]>(this.baseUrl);
  }

  getDrug(id): Observable<any> {
    return this.http.get<any>(this.baseUrl + id);
  }

  insertDrug(drug: Drug) {
    return this.http.post(this.baseUrl, drug);
  }

  updateDrug(drug: Drug) {
    return this.http.put(this.baseUrl, drug);
  }

  deleteDrug(drug: Drug) {
    return this.http.put(this.baseUrl + '/delete', drug);
  }


}
