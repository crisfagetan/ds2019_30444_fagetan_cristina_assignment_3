import { Component, OnInit } from '@angular/core';
import { Patient } from '../_models/patient';
import { CaregiverService } from '../_services/caregiver.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PatientService } from '../_services/patient.service';
import { AlertifyService } from '../_services/alertify.service';
import { Caregiver } from '../_models/caregiver';



@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {



  patient: Patient;
  caregiver: Caregiver;
  constructor(private patientService: PatientService, private caregiverService: CaregiverService,
              private alertify: AlertifyService, public router: Router, public route: ActivatedRoute) {
               }

  ngOnInit() {
    if (this.getRole() === 2) {
      this.showCurrentPatient();
    }
    if (this.getRole() === 1) {
    this.showCurrentCaregiver();
    }
  //  console.log(this.patients);
  }


  showCurrentPatient() {
    this.patientService.getPatient(+localStorage.getItem('userRoleId')).subscribe((patient: Patient) => {
     // console.log(patient);
      this.patient = patient;
     // console.log(this.patient);
    }, error => {
      this.alertify.error(error);
    });
  }

  showCurrentCaregiver() {
    this.caregiverService.getCaregiver(+localStorage.getItem('userRoleId')).subscribe((caregiver: Caregiver) => {
   //   console.log(caregiver);
      this.caregiver = caregiver;
     // console.log(this.caregiver);
    }, error => {
      this.alertify.error(error);
    });
  }

  // showCurrentDoctor() {
  //   this.caregiverService.getCaregiver(+localStorage.getItem('userRoleId')).subscribe((caregiver: Caregiver) => {
  //     console.log(caregiver);
  //     this.caregiver = caregiver;
  //     console.log(this.caregiver);
  //   }, error => {
  //     this.alertify.error(error);
  //   });
  // }


  getId(): string {
    return localStorage.getItem('userRoleId');
  }

  getRole(): number {
    return +localStorage.getItem('userRole');
  }

}
