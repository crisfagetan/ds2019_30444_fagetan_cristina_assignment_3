import { MedPlan } from './medPlan';

export class Patient {

    id: number;

    userId: number;

    firstName?: string;

    lastName?: string;

    dateOfBirth?: Date;

    gender?: boolean; // 1 = male; 0 = female

    address?: string;

    medicalRecord?: string;

    medPlans?: MedPlan[];
}
