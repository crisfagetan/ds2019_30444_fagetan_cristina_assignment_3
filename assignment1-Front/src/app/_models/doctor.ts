import { MedPlan } from './medPlan';

export interface Doctor {

  id: number;

  userId: number;

  firstName?: string;

  lastName?: string;

  dateOfBirth?: Date;

  gender?: boolean; // 1 = male; 0 = female

  address?: string;

  medPlans?: MedPlan[];
}
