export class UserRegister {

  id: number;

  username: string;

  password: string;

  role: number;  // 0 = doctor; 1 = caregiver; 2 = patient

  firstName?: string;

  lastName?: string;

  dateOfBirth: Date;

  gender: boolean; // 1 = male; 0 = female

  address?: string;
  // userInRoleId: number;
}
